import React, {useEffect, useState} from 'react';
import {SafeAreaView, View, Platform, StyleSheet} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import map from './screens/Maps';
import location from './screens/location';

const Stack = createStackNavigator();

const Tab = createBottomTabNavigator();
const MyStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={BottomTab}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="map"
          component={map}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const BottomTab = () => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'transparent'}}>
      <Tab.Navigator
        initialRouteName="Home"
        tabBarOptions={{
          activeTintColor: 'red',
          inactiveTintColor: 'blue',
          style: {
            // backgroundColor: states.ThemeReducer.themeData.bottomColor2,
          },
        }}>
        <Tab.Screen
          name="map"
          component={map}
          options={{
            tabBarLabel: 'Map',
            headerShown: false,
            tabBarIcon: ({tintColor}) => (
              <Icon
                name="map-marker"
                size={25}
                style={styles.icons}
                color='red'
              />
            ),
          }}
        />
        <Tab.Screen
          name="location"
          component={location}
          options={{
            tabBarLabel: 'Location',
            headerShown: false,
            tabBarIcon: ({tintColor}) => (
              <Icon
                name="google-maps"
                size={25}
                style={styles.icons}
                color='blue'
              />
            ),
          }}
        />
      </Tab.Navigator>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  icons: {
    // marginTop: Platform.OS === 'ios' ? hp('-1%') : hp('-1%'),
  },
});

export default MyStack;
