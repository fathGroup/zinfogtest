import React, {useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
// import Geolocation from '@react-native-community/geolocation';
const Map = ({navigation}) => {
  const mapRef = React.createRef();
  const [position, setPosition] = useState({
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.001,
    longitudeDelta: 0.001,
  });
  const savelocation = () => {
    fetch(
      'https://nominatim.openstreetmap.org/reverse?format=json&lat=10.6677032&lon=75.988872&zoom=18&addressdetails=2',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          latitude: position.latitude,
          longitude: position.longitude,
        }),
        //Request Type
      },
    )
      .then(response => response.json())
      .then(res => {
        console.log('ressss:', res);
        console.log('ressss:', res.lon);
      })
      .catch(error => {});
  };
  return (
    <View style={styles.container}>
      <MapView.Animated
        ref={mapRef}
        style={styles.mapStyle}
        rotateEnabled={true}
        initialRegion={position}
        onRegionChange={region => {
          setPosition(region);
          console.log(region);
        }}
        onPress={savelocation()}>
        <Marker
          title="Yor are here"
          description="This is a description"
          coordinate={position}
          onDragEnd={e => setPosition({x: e.nativeEvent.coordinate})}
        />
      </MapView.Animated>
    </View>
  );
};
const styles = StyleSheet.create({
  pressablestyle: {
    backgroundColor: 'white',
    padding: '2%',
    borderRadius: 10,
    marginTop: '15%',
    width: '40%',
    alignItems: 'center',
  },
  mapStyle: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    height: '100%',
    width: '100%',
  },
  container: {
    flex: 1,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
});
export default Map;
