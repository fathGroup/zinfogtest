import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Modal} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
const Location = ({navigation}) => {
  const [location, setlocation] = useState([]);
  const [lat, setlat] = useState('37.78825');
  const [place, setplace] = useState('kerala');

  const [lon, setlon] = useState('-122.4324');
  useEffect(() => {
    fetch(
      'https://nominatim.openstreetmap.org/reverse?format=json&lat=10.6677032&lon=75.988872&zoom=18&addressdetails=1',
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + global.refresh_token1,
        },

      },
    )
      .then(response => response.json())
      .then(res => {
        console.log('ressss:', res);
        console.log('ressss:', res.lon);
        setlat(res.lat);
        setlon(res.lon);
        setplace(res.building);
        setlocation(res);
      })
      .catch(error => {});
  }, []);

  const mark = () => {
    console.log('jjbvv');
    return (
      <Modal animationType="slide" transparent={true} visible={true}>
        <View style={{backgroundColor: 'red', flex: 1, width: '20%'}}>
          <Text
            style={[styles.modalText, {marginLeft: '30%', marginTop: '14%'}]}>
            PickUp Details
          </Text>
        </View>
      </Modal>
    );
  };
  return (
     <View style={styles.container}>
      <MapView
        style={styles.mapStyle}
        initialRegion={{
          latitude: Number(lat),
          longitude: Number(lon),
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}>
        <Marker
          title="kkk"
          description="vvv"
          onPress={mark()}
          coordinate={{
            latitude: Number(lat),
            longitude: Number(lon),
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        />
      </MapView>
    </View>
  );
};
const styles = StyleSheet.create({
  pressablestyle: {
    backgroundColor: 'white',
    padding: '2%',
    borderRadius: 10,
    marginTop: '15%',
    width: '40%',
    alignItems: 'center',
  },
  mapStyle: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    height: '100%',
    width: '100%',
  },
  container:{
    flex: 1,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  }
});
export default Location;
